<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Movie;
use Faker\Generator as Faker;

$factory->define(Movie::class, function (Faker $faker) {
    $photoArr = [
        'abstract',
        'city',
        'business',
        'people',
        'transport',
        'animals'
    ];

    return [
        'title' => $faker->sentence($nbWords = 2, $variableNbWords = true),
        'description' => $faker->paragraph($nbSentences = 5),
        'year' => $faker->numberBetween($min = 1958, $max = 2019),
        'photo' => $faker->imageUrl(50, 50, $faker->randomElement($photoArr)),
        'actor_id' => \App\Actor::inRandomOrder()->first()->id
    ];
});
