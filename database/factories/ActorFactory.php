<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Actor;
use Faker\Generator as Faker;

$factory->define(Actor::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'surname' => $faker->lastName,
        'year' => $faker->numberBetween($min = 1958, $max= 2015),
        'photo' => $faker->imageUrl(150, 150, 'people')
    ];
});
