<?php

use Illuminate\Database\Seeder;

class TodosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $todos =[
            [
                'Scrivere una lettera',
                'Vedere un film'
            ],
            [
                'Fare una corsa',
                'Fare la spesa'
            ]
        ];
        for ($i=0; $i < count($todos); $i++){
            foreach ($todos[$i] as $todo) {
                $this->save($i+1, $todo);
            }
        }
    }
    public function save($user_id, $name){
        $todo = new \App\Todo();
        $todo->user_id = $user_id;
        $todo->name = $name;
        $todo->save();
    }
}
