<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    public function users(){
        return $this->belongsToMany('App\User');
    }

    public function scopeNotPermission($query, $id){
        return $query->leftJoin(
            'permission_user', function ($join) use ($id){
                $join->on('permissions.id', '=', 'permission_user.permission_id')
                    ->where('permission_user.user_id', '=', $id);
        })
            ->whereNull('permission_user.permission_id');
    }

    public function scopeListPermission($query, $id){
        return $query->leftJoin(
            'permission_user', function ($join) use ($id){
            $join->on('permissions.id', '=', 'permission_user.permission_id')
                ->where('permission_user.user_id', '=', $id);
        });
    }
}
