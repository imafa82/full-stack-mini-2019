<?php

namespace App\Http\Controllers;

use App\Permission;
use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('users.index', ['users' => User::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }


    public function show(User $user)
    {
        if(Gate::denies('permission', 'editUser')){
            abort(403, "Non hai i permessi per modificare l'utente");
        }

        $permissions = Permission::notPermission($user->id)->select('permissions.id', 'permissions.name')->get();
        $permissionAll = Permission::listPermission($user->id)->select(array('permissions.id', 'permissions.name', 'permission_user.user_id'))->get();
//        foreach (Permission::all() as $perm){
//            $control = true;
//            foreach ($user->permissions as $p){
//                if($perm->id == $p->id){
//                    $control = false;
//                }
//            }
//            if($control){
//                array_push($permissions, $perm);
//            }
//        }

        return view('users.show', compact('user', 'permissions', 'permissionAll'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    public function update(Request $request, User $user)
    {

        //$user->permissions()->attach($request->input('permission'));
        $user->permissions()->sync($request->input('permissions'));
        return redirect()->route('users.show', $user->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
