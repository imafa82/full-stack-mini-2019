<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HelloController extends Controller
{
    public function index($name = "Massy", $surname= 'Salerno', $year=1982){
        $name = ucfirst($name);
        $surname = ucfirst($surname);
        return view('hello', compact('name', 'surname', 'year'));
        //['name' => $name, 'surname' => $surname]
    }
}
