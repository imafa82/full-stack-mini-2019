<?php

namespace App\Http\Controllers;

use App\Actor;
use App\Http\Requests\ActorRequest;
use App\Http\Traits\TestTrait;
use App\Http\Traits\WrapperCache;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class ActorController extends Controller
{
    use WrapperCache;

    public $nameCache;

    public function __construct()
    {
        $this->nameCache = 'actors';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $name = 'Lino';
        //dd(DB::select("SELECT * FROM actors WHERE name = ?", [$name]));
        //dd(DB::table('actors')->get());
        //dd(DB::table('actors')->select('name', 'surname')->get());
        //dd(DB::table('actors')->join('movies', 'actors.id', '=', 'movies.actor_id')->get());
        //dd(Actor::all('name', 'surname'));
//        if(Cache::has('actors')){
//            $cacheSave = Cache::get('actors');
//            dd($cacheSave);
//        }

        $actors = Cache::rememberForever($this->nameCache, function (){
            return Actor::all();
        });
//        Cache::store()->put('actors', $actors, 10);
        return view('actors.index', compact('actors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('actors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ActorRequest $request)
    {
//        $this->validate($request, [
//            'name' => 'required|alpha',
//            'surname' => 'required',
//            'year' => 'required'
//        ], [
//            'name.required' => 'Il campo nome è obbligatorio',
//            'name.alpha' => 'Il campo nome non può essere numerico',
//            'surname.required' => 'Il cognome è obbligatorio',
//            'year.required' => "Non puoi nascondere l'età"
//        ]);
        $actor = Actor::create($request->all());
        $this->removeList();
        return redirect()->route('actors.show', $actor->id);
    }


    public function show($id)
    {
        $actor = Cache::rememberForever($this->nameCache.'-'.$id,  function () use($id){
           return Actor::findOrFail($id)->load('movies');
        });
       // $actor = Actor::findOrFail($id);
       // dd($actor->movies);

        return view('actors.show', compact('actor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Actor  $actor
     * @return \Illuminate\Http\Response
     */
    public function edit(Actor $actor)
    {
        return view('actors.edit', compact('actor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\ActorRequest  $request
     * @param  \App\Actor  $actor
     * @return \Illuminate\Http\Response
     */
    public function update(ActorRequest $request, Actor $actor)
    {
//        $this->validate($request, [
//            'name' => 'required|alpha',
//            'surname' => 'required',
//            'year' => 'required'
//        ], [
//            'name.required' => 'Il campo nome è obbligatorio',
//            'name.alpha' => 'Il campo nome non può essere numerico',
//            'surname.required' => 'Il cognome è obbligatorio',
//            'year.required' => "Non puoi nascondere l'età"
//        ]);

        $data = $request->all();
        if ($request->hasFile('imageFile')) {
           $file = $request->file('imageFile');
           $fileName = $file->store('images');
           $data['photo'] = $fileName;
        }
        $actor->update($data);
        $this->removeList();
        $this->removeSingle($actor->id);
        return redirect()->route('actors.show', $actor->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Actor  $actor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Actor $actor)
    {
        //$actor->movies()->delete();
        $actor->delete();
        return redirect()->route('actors.index');
    }
}
