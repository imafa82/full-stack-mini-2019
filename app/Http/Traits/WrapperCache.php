<?php
namespace App\Http\Traits;

use Illuminate\Support\Facades\Cache;

trait WrapperCache{
    public function removeList(){
        $this->removeCache($this->nameCache);
    }

    public function removeSingle($id){
        $this->removeCache($this->nameCache.'-'.$id);
    }

    public function removeCache($name){
        Cache::forget($name);
    }
}
