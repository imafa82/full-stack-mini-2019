<?php

namespace App\Providers;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
         'App\Todo' => 'App\Policies\TodoPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('permission', function (User $user, $perm){
            //$control = false;

//            if(count($user->permissions)){
//                foreach ($user->permissions as $permission){
//                    if($permission->name == $perm)
//                        $control = true;
//                }
//            }

            return $user->getPermissionIsset($perm);
        });
    }
}
