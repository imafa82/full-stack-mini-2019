<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function permissions(){
        return $this->belongsToMany('App\Permission', 'permission_user', 'user_id', 'permission_id');
    }

    public function todos(){
        return $this->hasMany('App\Todo');
    }

    public function permissionIsset($perm){
        return $this->permissions()->where('name', '=', $perm);
    }

    public function getPermissionIsset($perm){
        return count($this->permissionIsset($perm)->get()) ? true : false;
    }

}
