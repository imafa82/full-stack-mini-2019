@extends('layouts.app')
@section('title', 'Crea nuovo attore')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Crea attore</div>

                    <div class="card-body">
                        <form action="{{route('actors.store')}}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label >Nome</label>
                                <input type="text" name="name" class="form-control" value="{{old('name')}}" placeholder="Nome attore">
                            </div>
                            <div class="form-group">
                                <label >Cognome</label>
                                <input type="text" name="surname" class="form-control" value="{{old('surname')}}" placeholder="Cognome attore">
                            </div>
                            <div class="form-group">
                                <label >Anno di nascita</label>
                                <input type="number" name="year" class="form-control" value="{{old('year')}}" placeholder="Data di nascita">
                            </div>
                            @if($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <button type="submit" class="btn btn-primary float-right">Salva</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
