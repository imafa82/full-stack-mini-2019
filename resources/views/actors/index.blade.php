@extends('layouts.app')
@section('title', 'Lista attori')

@section('content')
    <div class="container">
        <div class="row actor justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Lista attori</div>

                    <div class="card-body">
                        @foreach($actors as $actor)
                            <div class="row my-3 badge-primary">
                                <div class="col-3">
                                    @if($actor->photo)
                                     <img src="{{$actor->path}}" alt="{{$actor->name}} {{$actor->surname}}" class="img-thumbnail m-1">
                                    @endif
                                </div>
                                <div class="col-9">
                                    <div class="row">
                                        <div class="col-8">
                                            <h4>{{$actor->name}} {{$actor->surname}}</h4>
                                        </div>
                                        <div class="col-2">
                                            <a href="{{route('actors.show', $actor->id)}}">
                                                <i class="fa fa-address-card"></i>
                                            </a>
                                        </div>
                                        <div class="col-2">
                                            <form action="{{route('actors.destroy', $actor->id)}}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-primary-outline">
                                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        {{--{{$actors->links()}}--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
