@extends('layouts.app')
@section('title', $actor->name.' '.$actor->surname)

@section('content')
        <div class="container">
                <div class="row justify-content-center">
                        <div class="col-md-8">
                                <div class="card">
                                        <div class="card-header">
                                                Pagina di {{$actor->name}} {{$actor->surname}}
                                                <a href="{{route('actors.edit', $actor->id)}}" class="float-right">
                                                        <i class="fa fa-edit" aria-hidden="true"></i>
                                                </a>

                                        </div>

                                        <div class="card-body">
                                                <div class="row">
                                                        <div class="col-6">
                                                                <img src="{{$actor->path}}" style="max-width: 100%">
                                                        </div>
                                                        <div class="col-6">
                                                                <div class="row">
                                                                        <div class="col-12">
                                                                                Nome: {{$actor->name}}
                                                                        </div>
                                                                        <div class="col-12">
                                                                                Cognome: {{$actor->surname}}
                                                                        </div>
                                                                        <div class="col-12">
                                                                                Anno di nascita: {{$actor->year}}
                                                                        </div>
                                                                </div>
                                                        </div>
                                                </div>
                                                <div>
                                                        <h2>Lista film di {{$actor->name}} {{$actor->surname}}</h2>
                                                       @forelse($actor->movies as $movie)
                                                                <div class="row">
                                                                        <div class="col-8">{{$movie->title}}</div>
                                                                        <div class="col-2">
                                                                                <a href="{{route('movies.show', $movie->id)}}">
                                                                                        <i class="fa fa-eye"></i>
                                                                                </a>
                                                                        </div>
                                                                        <div class="col-2">
                                                                                <button  type="submit" data-id="{{$movie->id}}" class="movie-delete btn btn-primary-outline">
                                                                                        <i class="fa fa-trash"></i>
                                                                                </button>
                                                                        </div>
                                                                </div>
                                                        @empty
                                                                Non vi sono film registrati
                                                        @endforelse
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
@endsection

@section('javascript')

    <script>
        $(document).ready(function () {

            $('.movie-delete').click(function (e) {
               var id = $(this).data('id');
               var row = $(this).closest('.row');
               $.ajax({
                    url: '/movies/'+id,
                    type: 'DELETE',
                    success: function () {
                        console.log('ci siamo');
                        console.log(this);
                        row.remove();
                    },
                   error: function (err) {
                       console.log('errore');
                   }
            })

                console.log('non ci siamo')
            })
        })
    </script>
@endsection
