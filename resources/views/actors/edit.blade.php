@extends('layouts.app')
@section('title', 'Modifica '.$actor->name.' '.$actor->surname)
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Modifica {{$actor->name}} {{$actor->surname}}</div>

                    <div class="card-body">
                        <form action="{{route('actors.update', $actor->id)}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label >Nome</label>
                                <input type="text" name="name" class="form-control" value="{{$actor->name}}" placeholder="Nome attore">
                            </div>
                            <div class="form-group">
                                <label >Cognome</label>
                                <input type="text" name="surname" class="form-control" value="{{$actor->surname}}" placeholder="Cognome attore">
                            </div>
                            <div class="form-group">
                                <label >Anno di nascita</label>
                                <input type="number" name="year" class="form-control" value="{{$actor->year}}" placeholder="Data di nascita">
                            </div>
                            <div class="form-group">
                                <input type="file" name="imageFile">
                            </div>
                            @if($actor->photo)
                                <div class="p-2">
                                    <img width="300" src="{{$actor->path}}" alt="{{$actor->name}} {{$actor->surname}}">
                                </div>
                            @endif
                            @if($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <button type="submit" class="btn btn-primary float-right">Modifica</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
