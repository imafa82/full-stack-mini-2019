@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Home Js</div>

                <div class="card-body">

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
    <script>
        // {
        //     let test = 'Massimiliano';
        // }
        //
        // document.querySelectorAll('.card-body')[0].innerHTML = typeof test !== 'undefined' ? test : 'Qui non si legge la variabile test perchè let';

        // for(let i = 0; i<10; i++){
        //
        // }
        // console.log(i);
        // const mar = 'Marte';
        // console.log(mar);
        //
        // const testC = {
        //     name: 'Massimiliano',
        //     surname: 'Salerno'
        // }
        // testC.name = 'Mario';
        // console.log(testC);
        //
        // console.log(name);
        // var name = 'Massimiliano';


        // let newObj = {
        //     name: 'Massimiliano',
        //     surname: 'Salerno',
        //     array: [5, 6, 8, 9, 15],
        //     points: 35,
        //     print: function () {
        //         //console.log(this.name, this.surname);
        //         setTimeout(() => {
        //             console.log('avviata la nostra funzione')
        //             console.log(this.name, this.surname);
        //         }, 5000)
        //     },
        //     printArray: function () {
        //         //console.log(this.array);
        //         // this.array.forEach(function (ele) {
        //         //     console.log(ele);
        //         // })
        //     }
        // };
        // newObj.print();
        // newObj.printArray();
        //
        // var func = () => 2+2;
        // console.log(func());
        // var func2 = test => console.log(test);
        // func2('tttt');
        // var func3 = (name, surname) => console.log(name, surname);
        // func3(newObj.name, newObj.surname);

        var array = [
            {
                name: 'Patate',
                qt: '5Kg'
            },
            {
                name: 'Zucchine',
                qt: '1Kg'
            },
            {
                name: 'Carote',
                qt: '1Kg'
            }
        ];

        function test() {
            if(arguments.length){
                console.log('lista della spesa')
                for (let i = 0; i < arguments.length; i++){
                    console.log('Prodotto: ' + arguments[i].name);
                    console.log('Quantità: ' + arguments[i].qt);
                }
            }
        }


        var fruits= [
            {
                name: 'Fragole',
                qt: '1Kg'
            },
            {
                name: 'Banane',
                qt: '1Kg'
            },
            {
                name: 'Mele',
                qt: '1Kg'
            }
        ];

        var arrayPrint = [
            {
                name: 'Salmone',
                qt: '1Kg'
            },
            ...array,
            ...fruits,
            {
                name: 'Cozze',
                qt: '1Kg'
            },

        ];
        console.log(arrayPrint)
        //test(...arrayPrint);
        function sum(a = 1, b = 2) {
            return a+b;
        }
        console.log(sum());
        console.log(sum(3));
        console.log(sum(3, 5));
        var html = `
                <ul>
                     <li class="mark">Nome: ${newObj.name}</li>
                     <li class="mark">Cognome: ${newObj.surname}</li>
                     <li class="mark">Punteggio aggiornato: ${newObj.points + 5}</li>
                </ul>
           `;
        document.querySelectorAll('.card-body')[0].innerHTML = html;

        let array3 = ['Massimiliano', 'Salerno'];
        let [a, b] = array3;
        console.log(...array3);



        newObj = {
            name: 'Massimiliano',
            surname: 'Salerno',
            array: [5, 6, 8, 9, 15],
            address: {
              city: 'Roma',
              address: 'Piazza re di Roma, 7',
              cap: '00100'
            },
            points: 35,
        };


        let {name, surname : lastName} = newObj;
        console.log(name, lastName);

        let {address:{city, address}} = newObj;
        console.log(city, address);


        // name = 'Massimiliano';
        // let surname = 'Salerno';
        // let year = 1982;
        //
        // //let obj3 = {name: name, surname: surname, year: year};
        // let obj3 = {name, surname, year};
        // console.log(obj3);
    </script>
@endsection

