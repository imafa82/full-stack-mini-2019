@extends('layouts.app')
@section('title', 'Pagina di '.$user->name)

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Pagina di {{$user->name}}</div>

                    <div class="card-body">
                       <div class="row p-2">
                           <div class="col-6 p-2">
                               Nome: {{$user->name}}
                           </div>
                           <div class="col-6 p-2">
                               Email: {{$user->email}}
                           </div>
                       </div>
                        {{--<div>--}}
                            {{--<h2>Permessi dell'utente</h2>--}}
                            {{--@forelse($user->permissions as $permission)--}}
                                {{--<div class="row p-2 m1">--}}
                                    {{--<div class="col-10">--}}
                                        {{--{{$permission->name}}--}}
                                    {{--</div>--}}
                                    {{--<div class="col-2">--}}
                                        {{--<form action="{{route('users.permission.destroy', [$user->id, $permission->id])}}" method="POST">--}}
                                            {{--@csrf--}}
                                            {{--@method('DELETE')--}}
                                            {{--<button class="btn btn-primary-outline">--}}
                                                {{--<i class="fa fa-trash" aria-hidden="true"></i>--}}
                                            {{--</button>--}}
                                        {{--</form>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--@empty--}}
                                {{--L'utente non ha permessi--}}
                            {{--@endforelse--}}
                        {{--</div>--}}
                        <div class="row m-4">
                            <div class="col-12">
                                <h2>Tutti i permessi</h2>
                                <form action="{{route('users.update', $user->id)}}" method="POST">
                                    @csrf
                                    @method('PUT')
                                    @foreach($permissionAll as $p)
                                        <div class="form-group">
                                            <input type="checkbox" name="permissions[]" value="{{$p->id}}" {{$p->user_id ? 'checked' : ''}}>
                                            <label>{{$p->name}}</label>
                                        </div>
                                    @endforeach
                                    <button class="btn btn-primary float-right">Setta permessi</button>
                                </form>

                            </div>
                        </div>
                        {{--<div class="row">--}}
                            {{--<div class="col-12">--}}
                                {{--@if(count($permissions))--}}
                                    {{--<form action="{{route('users.update', $user->id)}}" method="POST">--}}
                                        {{--@csrf--}}
                                        {{--@method('PUT')--}}
                                        {{--<select name="permission">--}}
                                            {{--@foreach($permissions as $p)--}}
                                                {{--<option value="{{$p->id}}">{{$p->name}}</option>--}}
                                            {{--@endforeach--}}
                                        {{--</select>--}}
                                        {{--<button class="btn btn-primary float-right">Aggiungi Permesso</button>--}}
                                    {{--</form>--}}

                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
