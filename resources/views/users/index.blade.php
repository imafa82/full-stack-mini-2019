@extends('layouts.app')
@section('title', 'Lista utenti')

@section('content')
    <div class="container">
        <div class="row actor justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Lista utenti</div>

                    <div class="card-body">
                        @foreach($users as $user)
                            <div class="row my-3 badge-primary">
                                <div class="col-3">

                                </div>
                                <div class="col-9">
                                    <div class="row">
                                        <div class="col-8">
                                            <h4>{{$user->name}}</h4>
                                        </div>
                                        <div class="col-2">
                                            @can('permission', 'editUser')
                                            <a href="{{route('users.show', $user->id)}}">
                                                <i class="fa fa-address-card"></i>
                                            </a>
                                            @endcan
                                        </div>
                                        <div class="col-2">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
