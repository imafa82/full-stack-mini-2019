@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Caricamento in corso</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="row">
                        {{--@component('components.design.icon', [--}}
                            {{--'ico' => 'id-card-o',--}}
                            {{--'route' => 'actors.index',--}}
                            {{--'label' => 'Attori'--}}
                        {{--])--}}
                        {{--@endcomponent--}}
                        {{--@component('components.design.icon', [--}}
                            {{--'ico' => 'user',--}}
                            {{--'route' => 'users.index',--}}
                            {{--'label' => 'Utenti'--}}
                        {{--])--}}
                        {{--@endcomponent--}}
                        {{--@component('components.design.icon', [--}}
                            {{--'ico' => 'key',--}}
                            {{--'route' => 'permissions.index',--}}
                            {{--'label' => 'Permessi'--}}
                        {{--])--}}
                        {{--@endcomponent--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
    <script>
        $(function () {
            new window.pages.home();
        })

    </script>
@endsection
