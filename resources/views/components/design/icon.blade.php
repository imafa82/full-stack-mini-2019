<div class="col-6">
    <div class="iconHome">
        <a href="{{route($route)}}">
            <i class="fa fa-{{$ico}}"></i>
            <span>{{$label}}</span>
            {{$slot}}
        </a>
    </div>
</div>
