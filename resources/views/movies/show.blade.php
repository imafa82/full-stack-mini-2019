@extends('layouts.app')
@section('title', $movie->title)

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        {{$movie->title}}
                        <a href="{{route('movies.edit', $movie->id)}}" class="float-right">
                            <i class="fa fa-edit" aria-hidden="true"></i>
                        </a>

                    </div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-6">

                            </div>
                            <div class="col-6">
                                <div class="row">
                                    <div class="col-12">
                                        Titolo: {{$movie->title}}
                                    </div>
                                    <div class="col-12">
                                        Anno: {{$movie->year}}
                                    </div>
                                    <div class="col-12">
                                        Descrizione: {{$movie->description}}
                                    </div>
                                </div>
                                <div class="d-flex justify-content-center">
                                    <a href="{{route('actors.show', $movie->actor_id)}}" class="btn btn-primary">Lista Film</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
