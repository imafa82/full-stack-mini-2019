@extends('layouts.app')
@section('title', 'Modifica '.$movie->title)
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Modifica {{$movie->title}}</div>

                    <div class="card-body">
                        <form action="{{route('movies.update', $movie->id)}}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label >Titolo</label>
                                <input type="text" name="title" class="form-control" value="{{$movie->title}}" placeholder="Titolo film">
                            </div>
                            <div class="form-group">
                                <label >Anno</label>
                                <input type="text" name="year" class="form-control" value="{{$movie->year}}" placeholder="Anno film">
                            </div>
                            <div class="form-group">
                                <label >Descrizione</label>
                                <textarea class="form-control" name="description" placeholder="Descrizione film">{{$movie->description}}</textarea>
                            </div>
                            @if($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="d-flex justify-content-around">
                                <a href="{{route('movies.show', $movie->id)}}" class="btn btn-primary"> Annulla</a>
                                <button type="submit" class="btn btn-primary float-right">Modifica</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
