class Home {
    constructor(){
        // console.log('siamo dentro Home');
        // var arr = [];
        // var p = new Promise(function (resolve, reject) {
        //     setTimeout(() => {
        //         var num = Math.round((10 * Math.random()));
        //         console.log(num, '5000');
        //         if(num > 0){
        //             resolve(num)
        //         } else {
        //             reject("chiamata fallita il numero era " + num)
        //         }
        //
        //     }, 5000)
        // });
        //
        //
        // var p2 = new Promise(function (resolve, reject) {
        //     setTimeout(() => {
        //         var num = Math.round((10 * Math.random()));
        //         console.log(num, '2000');
        //         resolve(num);
        //     }, 2000)
        // })


        // p.then( res => {
        //     console.log(res);
        //     arr.push(res);
        //     // let sum = 0;
        //     // arr.forEach(el => {
        //     //    sum += el;
        //     // });
        //     let sum = arr.reduce((sum, el) =>{
        //         sum += el;
        //         return sum;
        //     }, 0);
        // }).catch(err => {
        //     console.log(err);
        // })
        //
        // p2.then(res => {
        //     console.log(res);
        //     arr.push(res);
        // })

        // Promise.all([p, p2]).then(res => {
        //     let sum = res.reduce((sum, el) =>{
        //                 sum += el;
        //                 return sum;
        //             }, 0);
        //     console.log(sum);
        // })


        let url = 'https://jsonplaceholder.typicode.com/albums/2';
        let album = fetch(url).then(res => res.json());
        let photos = fetch(url + '/photos').then(res => res.json());
        Promise.all([album, photos]).then(([album, photos]) => {
           // let album = res[0];
           // let photos = res[1];
            this.printAlbumData(album);

            this.printListPhotos(photos);
        });




    }

    printAlbumData(album){
        let html = `<h2>${album.title}</h2>`
        document.querySelector('.card-header').innerHTML = html;
    }

    printListPhotos(photos){
        let photosHtml = '<ul>';
        photos.forEach(ele => {
            photosHtml +=  `
                                <li>
                                    <figure>
                                        <img src="${ele.thumbnailUrl}">
                                        <figcaption>${ele.title}</figcaption>
                                    </figure>
                                </li>
                            `
        });
        photosHtml += '</ul>'
        document.querySelector('.card-body').innerHTML = photosHtml;
    }
}

export default Home;
