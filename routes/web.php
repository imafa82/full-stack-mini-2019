<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/ciao/{name?}/{surname?}/{year?}', 'HelloController@index')->name('hello')
    ->where('name', '[a-z,A-Z]+')
    ->where('surname', '[a-z]+')
    ->where('year', '[0-9]+');

//Route::resource('table', 'TableController')->only('index');

Route::get('table/{n?}', 'TableController@index')->where('n', '[0-9]+');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function (){
    Route::resource('actors', 'ActorController');
    Route::resource('movies', 'MovieController')->only('show', 'edit', 'update', 'destroy');
    Route::resource('permissions', 'PermissionController');
    Route::resource('users', 'UserController');
    Route::resource('todos', 'TodoController');
    Route::resource('users.permission', 'PermissionUserController');
    Route::get('/homejs', function (){
        return view('homejs');
    });
});





//Route::get('actors', 'ActorController@index');
//Route::post('actors', 'ActorController@store');
//Route::get('actors/{id}', 'ActorController@show');
//Route::put('actors/{id}', 'ActorController@update');
//Route::delete('actors/{id}', 'ActorController@destroy');
//
//Route::get('actors/create', 'ActorController@create');
//Route::get('actors/{id}/edit', 'ActorController@edit');
